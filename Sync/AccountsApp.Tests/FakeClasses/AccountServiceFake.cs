﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AccountsApp;

namespace AccountsApp.Tests.FakeClasses
{
    internal class AccountServiceFake : IAccountService
    {
        private readonly Dictionary<int, double> _amounts = new Dictionary<int, double>();

        public AccountServiceFake(Dictionary<int, double> amounts)
        {
            _amounts = amounts;
        }

        public double GetAccountAmount(int accountId)
        {
            if (!_amounts.ContainsKey(accountId))
            {
                throw new KeyNotFoundException("Account identifier not found");
            }

            return _amounts[accountId];
        }
    }
}
