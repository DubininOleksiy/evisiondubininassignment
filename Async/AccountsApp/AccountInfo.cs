﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AccountsApp
{
    public class AccountInfo
    {
        private readonly int _accountId;
        private readonly IAccountService _accountService;

        private double _amount;

        public event EventHandler<RefreshAmountEventArgs> RefreshAmountCompleted;

        public AccountInfo(int accountId, IAccountService accountService)
        {
            _accountId = accountId;
            _accountService = accountService;
        }

        public double Amount
        {
            get
            {
                return Interlocked.Exchange(ref _amount, _amount);
            }
        }

        public void RefreshAmount()
        {
            RefreshAmountAsync();
        }

        protected virtual void OnRefreshAmountCompleted(Task<double> task)
        {
            EventHandler<RefreshAmountEventArgs> temp = RefreshAmountCompleted;

            if (temp != null)
            {
                temp(this, new RefreshAmountEventArgs(task));
            }
        }

        private Task<double> RefreshAmountAsync()
        {
            return Task.Factory
                .StartNew(() => _accountService.GetAccountAmount(_accountId))
                .ContinueWith(t =>
                {
                    if (t.IsFaulted)
                    {
                        OnRefreshAmountCompleted(t);
                        
                        return double.NaN;
                    }
                    else
                    {
                        double result = t.Result;

                        Interlocked.Exchange(ref _amount, result);
                        
                        OnRefreshAmountCompleted(t);

                        return result;
                    }
                });
        }
    }
}
