﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountsApp
{
    public class RefreshAmountEventArgs : EventArgs
    {
        public Task<double> Task { get; set; }

        public RefreshAmountEventArgs(Task<double> task)
        {
            Task = task;
        }
    }
}
